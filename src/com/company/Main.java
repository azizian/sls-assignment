package com.company;

import java.util.Arrays;
import java.util.List;

public class Main {


    public static boolean findNums(int[] list,int k){
        for(int i=0;i<list.length;i++){
            int x=list[i];
            int result=Arrays.binarySearch(list,k-x);
            if(result>0)
                return true;
        }
        return false;
    }

    public static void main(String[] args) {
        int [] ll={2,3,6,1};
        System.out.println(findNums(ll,1));
        // write your code here
    }
}
